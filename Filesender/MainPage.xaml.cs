﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Threading;
using Windows.System.Threading;
using Windows.UI.Core;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Filesender
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private FileOpenPicker _fileOpenPicker = null;

        private ObservableCollection<ListViewItem> _currentlySelectedFile = new ObservableCollection<ListViewItem>();

        public MainPage()
        {
            this.InitializeComponent();
            _currentlySelectedFile.CollectionChanged += OnCollectionChanged;
            MainGridView.ItemsSource = _currentlySelectedFile;
           
            
            InitTimer();

            OpenFilePicker();
        }

        private void InitTimer()
        {
            TimeSpan period = TimeSpan.FromSeconds(1);

            ThreadPoolTimer periodicTimer = ThreadPoolTimer.CreatePeriodicTimer(async (source) =>
            {
                
                long size = GetTotalFileSize();
                

                // Update the UI thread by using the UI core dispatcher.
                
                await Dispatcher.RunAsync(CoreDispatcherPriority.Low,
                    () =>
                    {
                        
                        // UI components can be accessed within this scope.
                        if (_currentlySelectedFile.Count > 0)
                        {
                            //update the total size
                            TotalFileSize_label.Text = "Size in total: " + size.ToString() + " Bytes";

                        }
                        else
                        {
                            TotalFileSize_label.Text = "N/A Bytes";

                        }
                        

                    });

            }, period);
        }

        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (_currentlySelectedFile.Count > 0)
            {
                NoFiles_textblock.Visibility = Visibility.Collapsed;
                MainGridView.Visibility = Visibility.Visible;
            }
            else
            {
                NoFiles_textblock.Visibility = Visibility.Visible;
                MainGridView.Visibility = Visibility.Collapsed;
            }

            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                //  MainGridView.Items?.Add(e.NewItems);
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                if (_currentlySelectedFile.Count > 0)
                {
                    NoFiles_textblock.Visibility = Visibility.Collapsed;
                    MainGridView.Visibility = Visibility.Visible;
                }
                else
                {
                    NoFiles_textblock.Visibility = Visibility.Visible;
                    MainGridView.Visibility = Visibility.Collapsed;
                }
            }
        }


        private async void OpenFilePicker()
        {
            _fileOpenPicker = new FileOpenPicker
            {
                ViewMode = PickerViewMode.Thumbnail,
                FileTypeFilter = {"*"}
            };
            var files = await _fileOpenPicker.PickMultipleFilesAsync();
            if (files.Count > 0)
            {
                foreach (var file in files)
                {
                    var fileProperties = await file.GetBasicPropertiesAsync();
                    var fileSize = fileProperties.Size;
                    _currentlySelectedFile.Add(new ListViewItem(file, file.Name, (int) fileSize));
                }
            }
        }

      

        private long GetTotalFileSize()
        {
            long totalFileSize = 0;
            if (_currentlySelectedFile.Count > 0)
            {
                foreach (var currentFile in _currentlySelectedFile)
                {
                    totalFileSize = totalFileSize + currentFile.Filesize;
                }
            }

            return totalFileSize;
        }

        private void OK_button_Click(object sender, RoutedEventArgs e)
        {
            OpenFilePicker();
        }
    }
}