﻿using Windows.Storage;

namespace Filesender
{
    public class ListViewItem
    {
        private StorageFile File { get; set; }

        public string Filename { get; set; }

        public long Filesize { get; set; }

        public string Filesizestring { get; set; }

        public ListViewItem(StorageFile file, string fileName, int fileSize)
        {
            File = file;
            Filename = fileName;
            Filesizestring = fileSize + " Bytes";
            Filesize = fileSize;
        }
    }
}