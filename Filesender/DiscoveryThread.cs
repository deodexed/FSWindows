﻿using System;
using System.Net;
using Windows.ApplicationModel.Background;
using Windows.Networking.Sockets;

namespace Filesender
{
    public class DiscoveryThread : IBackgroundTask
    {

        private const int _sendingPort = 5557;

        private bool isPaused = false;

        private bool isStoped = false;

        private DatagramSocket _datagramSocket;

        private IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Broadcast, _sendingPort);
        
        public DiscoveryThread()
        {
            this._datagramSocket = new DatagramSocket();
        }

        private async void BindDatagramSocket()
        {
            await this._datagramSocket.ConnectAsync("255.255.255.255");

        }

        public void Run(IBackgroundTaskInstance taskInstance)
        {
            while (!isStoped)
            {
                if (!isPaused)
                {
                    SendBeacons();
                }
            }
        }

        private void SendBeacons()
        {

        }
    }
}